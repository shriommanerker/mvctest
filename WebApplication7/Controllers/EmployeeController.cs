﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Models;


namespace WebApplication7.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        private readonly ApplicationDbContext db = new ApplicationDbContext();
        // GET: Employee
        public ActionResult Index()
        {

            List<Employee> employeeList = db.Employees.ToList();

            //List<Employee> employeeList = new List<Employee>
            //{ new Employee { Name = "Shriom", City = "Margao", Gender = "Male", Id = 1 } ,
            // new Employee { Name = "Shruti", City = "Pune", Gender = "Female", Id = 2 } };

            return View(employeeList);
        }
        
        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            Employee employee = db.Employees.FirstOrDefault(e => e.Id == id);

            return View(employee);
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(Employee e)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Employees.Add(e);
                    db.SaveChanges();
                }
                else
                {
                    ModelState.AddModelError("", "Error ");
                }
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            Employee employee = db.Employees.FirstOrDefault(e => e.Id == id);


            return View(employee);
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(EditEmployeeViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                }
                else
                {
                    ModelState.AddModelError("", "Failed to verify phone");
                }
                // TODO: Add update logic here

                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
